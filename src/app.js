const express = require("express");
const bodyParser = require("body-parser");
const { sequelize } = require("./model");
const { getProfile } = require("./middleware/getProfile");
const {
  CONTRACT_STATUSES,
  PROFILE_TYPES,
  deepCopy,
  getAllContracts,
  getUpaidedContracts,
  getJobFromProfile,
  getTotalPayedAmount,
  getTopClients,
  getTopProfessions,
  isProfileContract,
  isEmpty,
  safeGetValue,
} = require("./utils");
const app = express();
app.use(bodyParser.json());
app.set("sequelize", sequelize);
app.set("models", sequelize.models);
app.use(getProfile);

/**
 * FIX ME!
 * @returns contract by id
 */
app.get("/contracts/:id", async (req, res) => {
  const { Contract } = req.app.get("models");
  const { id } = req.params;
  const contract = await Contract.findOne({ where: { id } });
  if (!contract || !isProfileContract(contract, req.profile))
    return res.status(404).end();
  res.json(contract);
});

app.get("/contracts", async (req, res) => {
  const { Contract } = req.app.get("models");
  const contracts = await getAllContracts(Contract, req.profile);
  if (isEmpty(contracts)) return res.status(404).end();
  res.json(contracts);
});

app.get("/jobs/unpaid", async (req, res) => {
  const models = req.app.get("models");
  const contracts = await getUpaidedContracts(models, req.profile);
  if (isEmpty(contracts)) return res.status(404).end();
  res.json(contracts);
});

app.post("/jobs/:job_id/pay", async (req, res) => {
  // if a deposit is maded at the same time, we will have problems
  // because we are using the balance getted at the beggining of this request
  // to know if the client is able to pay a job.
  const models = req.app.get("models");
  const {
    profile,
    params: { job_id: jobId },
  } = req;
  if (profile.type !== PROFILE_TYPES.CLIENT) return res.status(401).end();
  const job = await getJobFromProfile(models, jobId, profile);
  // return that the job is not able to paid, because it not accomplish the requirements
  // ( needs to be unpaided and related to the client)
  if (isEmpty(job)) return res.status(404).end();
  if (job.Contract.Client.balance < job.price) return res.status(402).end();

  const tempJob = deepCopy(job);
  try {
    job.Contract.Contractor.balance =
      job.Contract.Contractor.balance + job.price;
    job.Contract.Client.balance = job.Contract.Client.balance - job.price;
    job.paid = true;
    job.Contract.status = CONTRACT_STATUSES.TERMINATED;
    await job.Contract.Contractor.save();
    await job.Contract.Client.save();
    await job.Contract.save();
    await job.save();
  } catch (e) {
    job.Contract.Contractor.balance = tempJob.Contract.Contractor.balance;
    job.Contract.Client.balance = tempJob.Contract.Client.balance;
    job.paid = tempJob.paid;
    job.Contract.status = tempJob.Contract.status;
    await job.Contract.Contractor.save();
    await job.Contract.Client.save();
    await job.Contract.save();
    await job.save();

    return res.status(500).end();
  }
  res.json(job);
});

app.post("/balances/deposit/:userId", async (req, res) => {
  const models = req.app.get("models");
  const {
    profile,
    params: { userId },
    query: { amount: stringAmount },
  } = req;
  const amountToDeposit = Number(stringAmount);
  // here we are going to validate that the account to perform a deposit needs to be
  // the same account that the one that is making the request.
  if (profile.type !== PROFILE_TYPES.CLIENT || profile.id !== Number(userId))
    return res.status(401).end();

  const totalPayedAmount = await getTotalPayedAmount(models, profile);
  // the client haven't any job pending to pay.
  if (!totalPayedAmount) return res.status(400).end();

  //  a client can't deposit more than 25% his total of jobs to pay
  if (amountToDeposit > 0.25 * totalPayedAmount) return res.status(501).end();

  const tempBalance = profile.balance;
  try {
    profile.balance = profile.balance + amountToDeposit;
    await profile.save();
  } catch (e) {
    profile.balance = tempBalance;
    await profile.save();
    // there was a problem on the UPDATE transaction
    return res.status(500).end();
  }
  res.json(profile);
});

app.get("/admin/best-profession", async (req, res) => {
  const models = req.app.get("models");
  const {
    profile,
    query: { start: startString, end: endString },
  } = req;
  const startDate = new Date(startString);
  const endDate = new Date(endString);
  if (isNaN(startDate.getTime()) || isNaN(endDate.getTime()))
    return res.status(400).end();
  // added a new profile type called admin, to only allow them to perform the following actions
  if (profile.type !== PROFILE_TYPES.ADMIN) return res.status(401).end();

  const professions = await getTopProfessions(models, startDate, endDate);
  if (isEmpty(professions)) return res.status(404).end();
  res.json({
    profession: safeGetValue("profession", professions[0]),
    totalAmount: safeGetValue("totalAmount", professions[0]),
  });
});

app.get("/admin/best-clients", async (req, res) => {
  const models = req.app.get("models");
  const {
    profile,
    query: { start: startString, end: endString, limit: limitString },
  } = req;
  const limit = limitString ? Number(limitString) : 2;
  const startDate = new Date(startString);
  const endDate = new Date(endString);
  if (isNaN(limit) || isNaN(startDate.getTime()) || isNaN(endDate.getTime()))
    return res.status(400).end();
  // added a new profile type called admin, to only allow them to perform the following actions
  if (profile.type !== PROFILE_TYPES.ADMIN) return res.status(401).end();

  // We cannot apply a limit field to this `findAll` because breaks thre query.
  // more info related: https://github.com/sequelize/sequelize/issues/8110
  const topClients = await getTopClients(models, startDate, endDate);
  // As there aren't so many recs we are going to apply the limit on the service side,
  // but to improve the perfomance, we need to change the above line to apply
  // a raw query with the specific limit. See more info: https://sequelize.org/docs/v6/core-concepts/raw-queries/
  if (isEmpty(topClients)) return res.status(404).end();
  res.json(
    topClients.slice(0, limit).map((item) => ({
      totalAmount: safeGetValue("totalAmount", item),
      ...(item.Client.dataValues || {}),
    }))
  );
});

module.exports = app;
