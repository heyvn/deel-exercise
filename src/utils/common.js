const isProfileContract = (contract, profile) => {
  switch (profile.type) {
    case "contractor":
      return contract.ContractorId === profile.id;
    case "client":
      return contract.ClientId === profile.id;
    default:
      throw new Error("contractor type is not allowed");
  }
};

const isEmpty = (value) => {
  return !value || (Array.isArray(value) && value.length === 0);
};

const deepCopy = (object) => {
  // this isn't the best way to perform a deep copy, but taking
  // in consideration the complexity of the objects to copy, will be fine.
  return JSON.parse(JSON.stringify(object));
};

const safeGetValue = (field, object) => {
  return (
    object[field] ||
    (object.dataValues && object.dataValues[field]) ||
    undefined
  );
};

module.exports = {
  deepCopy,
  isProfileContract,
  isEmpty,
  safeGetValue,
};
