const PROFILE_TYPES = {
  CONTRACTOR: "contractor",
  CLIENT: "client",
  ADMIN: "admin",
};

const FIELD_NAMES_BY_PROFILE_TYPES = {
  [PROFILE_TYPES.CONTRACTOR]: "ContractorId",
  [PROFILE_TYPES.CLIENT]: "ClientId",
};

const CONTRACT_STATUSES = {
  NEW: "new",
  IN_PROGRESS: "in_progress",
  TERMINATED: "terminated",
};

module.exports = {
  CONTRACT_STATUSES,
  FIELD_NAMES_BY_PROFILE_TYPES,
  PROFILE_TYPES,
};
