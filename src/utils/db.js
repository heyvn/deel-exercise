const { FIELD_NAMES_BY_PROFILE_TYPES, PROFILE_TYPES } = require("./constants");
const { Op } = require("sequelize");
const { sequelize } = require("../model");

async function getAllContracts(Contract, { type, id }) {
  return await Contract.findAll({
    where: {
      [FIELD_NAMES_BY_PROFILE_TYPES[type]]: id,
      status: {
        [Op.not]: CONTRACT_STATUSES.TERMINATED,
      },
    },
  });
}

async function getUpaidedContracts({ Job, Contract }, { type, id }) {
  return await Job.findAll({
    where: {
      paid: {
        [Op.not]: true,
      },
    },
    include: {
      model: Contract,
      required: true,
      where: {
        [FIELD_NAMES_BY_PROFILE_TYPES[type]]: id,
        status: CONTRACT_STATUSES.IN_PROGRESS,
      },
    },
  });
}

async function getJobFromProfile({ Contract, Job, Profile }, jobId, profile) {
  return await Job.findOne({
    where: {
      id: jobId,
      paid: {
        [Op.not]: true,
      },
    },
    include: {
      model: Contract,
      required: true,
      where: {
        [FIELD_NAMES_BY_PROFILE_TYPES.client]: profile.id,
        status: CONTRACT_STATUSES.IN_PROGRESS,
      },
      include: [
        {
          model: Profile,
          required: true,
          as: "Contractor",
        },
        {
          model: Profile,
          required: true,
          as: "Client",
        },
      ],
    },
  });
}

async function getTotalPayedAmount({ Job, Contract }, profile) {
  return await Job.sum("price", {
    where: {
      paid: {
        [Op.not]: true,
      },
    },
    include: {
      model: Contract,
      required: true,
      where: {
        [FIELD_NAMES_BY_PROFILE_TYPES.client]: profile.id,
        status: {
          [Op.not]: CONTRACT_STATUSES.TERMINATED,
        },
      },
    },
  });
}

async function getTopClients({ Contract, Job, Profile }, startDate, endDate) {
  return await Contract.findAll({
    include: [
      {
        model: Job,
        where: {
          paid: true,
          paymentDate: {
            [Op.gte]: startDate,
            [Op.lte]: endDate,
          },
        },
      },
      { model: Profile, as: "Client" },
    ],
    group: "ClientId",
    attributes: [
      [sequelize.fn("SUM", sequelize.col("Jobs.price")), "totalAmount"],
    ],
    order: [["totalAmount", "DESC"]],
  });
}

async function getTopProfessions(
  { Profile, Contract, Job },
  startDate,
  endDate
) {
  return await Profile.findAll({
    include: {
      model: Contract,
      required: true,
      as: "Contractor",
      include: {
        model: Job,
        required: true,
        where: {
          paid: true,
          paymentDate: {
            [Op.gte]: startDate,
            [Op.lte]: endDate,
          },
        },
      },
    },
    where: {
      type: PROFILE_TYPES.CONTRACTOR,
    },
    group: "profession",
    attributes: [
      "profession",
      [
        sequelize.fn("SUM", sequelize.col("Contractor.Jobs.price")),
        "totalAmount",
      ],
    ],
    order: [["totalAmount", "DESC"]],
  });
}

module.exports = {
  getAllContracts,
  getUpaidedContracts,
  getJobFromProfile,
  getTotalPayedAmount,
  getTopClients,
  getTopProfessions,
};
